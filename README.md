# React Chess
[Try demo](https://react-chess-f5afc.web.app/)

## Functionalities
- Start places white pawn to random legal position
- Shows the current FEN of the game
- Move black or white pawn, queen, rook, bishop, or knight
- Promotion
- Capture other piece

## Running Locally
### Clone
```
git clone https://gitlab.com/Lankinen/react-chess
cd react-chess
```
### Install Required Packages
```
npm install
```
### Run
```
npm start
```
Opens automatically [http://localhost:3000](http://localhost:3000).
### Tests
```
npm test
```
### Build
```
npm run-script build
```