const emptyArray = (size: number, repeat?: any) => {
  let a = [...new Array(size)].map((_, i: number) => i);
  if (repeat) {
    a = a.map(() => repeat);
  }
  return a;
};

const shuffleArray = (arr: any[]) => arr.sort(() => Math.random() - 0.5);

const cloneArray = (arr: any[]) => arr.map((item) => item);

const cloneDoubleArray = (arr: any[][]) => arr.map((row) => row.map((item) => item));

export {
  emptyArray,
  shuffleArray,
  cloneArray,
  cloneDoubleArray,
};
