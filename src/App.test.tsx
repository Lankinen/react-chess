import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText('8/2p5/8/8/8/8/8/8 w KQkq - 0 1');
  expect(linkElement).toBeInTheDocument();
});
