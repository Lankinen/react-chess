enum GameRunning {
  STOP,
  START,
  BLACK_WON,
  WHITE_WON,
  PROMOTION
}

export default GameRunning;
