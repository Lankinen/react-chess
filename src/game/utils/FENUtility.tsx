import { emptyArray, cloneArray } from '../../utils/ArrayUtils';

export const FENPositionsToArray = (FENPositions: string[]) => FENPositions.map((rowString) => rowString
  .split('')
  .map((i) => (parseInt(i, 10) ? emptyArray(parseInt(i, 10), '0').join(',') : i))
  .join(',')
  .split(','));

export const arrayPositionsToFEN = (arrayPositions: string[][]) => {
  let counter = 0;
  return arrayPositions
    .map((row) => {
      let res: string = row
        .map((item) => {
          if (item === '0') {
            counter += 1;
          } else if (counter > 0) {
            const value = `${counter}${item}`;
            counter = 0;
            return value;
          } else {
            const value = `${item}`;
            return value;
          }
          return '';
        })
        .join('');
      if (counter > 0) {
        res = `${res}${counter}`;
        counter = 0;
      }
      return res;
    })
    .join('/');
};

export const updateTurn = (gameState: string[]) => {
  const _gameState = cloneArray(gameState);
  _gameState[0] = _gameState[0] === 'w' ? 'b' : 'w';
  return _gameState;
};

export const updateNumberOfMoves = (gameState: string[], capture: boolean = false) => {
  const _gameState = cloneArray(gameState);
  let numOfMovesSinceCapture: number = parseInt(_gameState[3], 10);
  let moveNumber: number = parseInt(_gameState[4], 10);
  if (capture) {
    numOfMovesSinceCapture = 0;
  } else {
    numOfMovesSinceCapture += 1;
  }
  moveNumber += 1;
  _gameState[3] = numOfMovesSinceCapture.toString();
  _gameState[4] = moveNumber.toString();
  return _gameState;
};
