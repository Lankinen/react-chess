import {
  pawnMoves,
  queenMoves,
  rookMoves,
  bishopMoves,
  knightMoves,
} from './PieceMoves';
import {
  cloneDoubleArray,
} from '../../utils/ArrayUtils';

export const isWhiteSquare = (rowIndex: number, colIndex: number) => {
  if (rowIndex % 2 === 0) {
    return colIndex % 2 === 0;
  }
  return colIndex % 2 !== 0;
};

export const isWhitePiece = (piece: string) => piece === piece.toUpperCase();

export const setPromotionToBoard = (positions: string[][], promotionPiece: string) => {
  const newPositions = positions.map((row) => row.map((item) => (item === '?' ? promotionPiece : item)));
  return newPositions;
};

export const isEmptySquare = (
  positions: string[][],
  rowIndex: number,
  colIndex: number,
) => positions[rowIndex][colIndex] === '0';

export const isPromotion = (rowIndex: number, piece: string) => {
  const isEndOfBoard = (rowIndex === 0 || rowIndex === 7);
  return piece.toLowerCase() === 'p' && isEndOfBoard;
};

export const movePiece = (
  positions: string[][],
  fromRowIndex: number,
  fromColumnIndex: number,
  toRowIndex: number,
  toColumnIndex: number,
) => {
  const _positions = cloneDoubleArray(positions);
  let piece: string = _positions[fromRowIndex][fromColumnIndex];
  _positions[fromRowIndex][fromColumnIndex] = '0';
  if (isPromotion(toRowIndex, piece)) {
    // Promotion
    piece = '?';
  }
  _positions[toRowIndex][toColumnIndex] = piece;
  return _positions;
};

export const isLegalMove = (
  positions: string[][],
  turn: string,
  selected: string,
  checkRowIndex: number,
  checkColIndex: number,
) => {
  if (selected) {
    const selectedPiece: string[] = selected.split('');
    const pieceType: string = selectedPiece[0].toLowerCase();
    const currentRowIndex: number = parseInt(selected[1], 10);
    const currentColIndex: number = parseInt(selected[2], 10);

    let possibleMoves: string[] = [];
    if (pieceType === 'p') {
      possibleMoves = pawnMoves(
        positions,
        turn === 'w',
        currentRowIndex,
        currentColIndex,
      );
    } if (pieceType === 'q') {
      possibleMoves = queenMoves(positions, currentRowIndex, currentColIndex);
    } if (pieceType === 'r') {
      possibleMoves = rookMoves(positions, currentRowIndex, currentColIndex);
    } if (pieceType === 'b') {
      possibleMoves = bishopMoves(positions, currentRowIndex, currentColIndex);
    } if (pieceType === 'n') {
      possibleMoves = knightMoves(currentRowIndex, currentColIndex);
    }
    return possibleMoves.includes(`${checkRowIndex}${checkColIndex}`);
  }
  return false;
};
