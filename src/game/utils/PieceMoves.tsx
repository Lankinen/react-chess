import { isEmptySquare } from './GameUtility';

export const pawnMoves = (
  positions: string[][],
  isWhite: boolean,
  rowIndex: number,
  colIndex: number,
) => {
  const possibleMoves: string[] = [];

  const mult = isWhite ? 1 : -1;
  const nextRow = rowIndex - 1 * mult;
  if (nextRow > 7 || nextRow < 0) {
    return possibleMoves;
  }
  // Forward
  if (isEmptySquare(positions, nextRow, colIndex)) {
    possibleMoves.push(`${nextRow}${colIndex}`);
  }
  // Initial Forward
  if ((isWhite && rowIndex === 6) || (!isWhite && rowIndex === 1)) {
    const nextNextRow = rowIndex - 2 * mult;
    if (isEmptySquare(positions, nextNextRow, colIndex)) {
      possibleMoves.push(`${nextNextRow}${colIndex}`);
    }
  }
  // Diagonal
  let newCol = colIndex - 1;
  if (newCol >= 0) {
    if (!isEmptySquare(positions, nextRow, newCol)) {
      possibleMoves.push(`${nextRow}${newCol}`);
    }
  }
  newCol = colIndex + 1;
  if (newCol <= 7) {
    if (!isEmptySquare(positions, nextRow, newCol)) {
      possibleMoves.push(`${nextRow}${newCol}`);
    }
  }
  return possibleMoves;
};

const directionsHelper = (directions: number[][], positions: string[][], rowIndex: number, colIndex: number) => {
  const possibleMoves: string[] = [];
  directions.forEach((direction) => {
    const rowMult: number = direction[0];
    const colMult: number = direction[1];
    let nextRow = rowIndex + (1 * rowMult);
    let nextCol = colIndex + (1 * colMult);
    while (nextRow >= 0 && nextRow <= 7 && nextCol >= 0 && nextCol <= 7) {
      possibleMoves.push(`${nextRow}${nextCol}`);
      if (!isEmptySquare(positions, nextRow, nextCol)) {
        break;
      }
      nextRow += (1 * rowMult);
      nextCol += (1 * colMult);
    }
  });
  return possibleMoves;
};

export const queenMoves = (
  positions: string[][],
  rowIndex: number,
  colIndex: number,
) => {
  const directions: number[][] = [[-1, 0], [1, 0], [0, -1], [0, 1], [-1, -1], [-1, 1], [1, -1], [1, 1]];
  const possibleMoves: string[] = directionsHelper(directions, positions, rowIndex, colIndex);
  return possibleMoves;
};

export const rookMoves = (
  positions: string[][],
  rowIndex: number,
  colIndex: number,
) => {
  const directions = [[-1, 0], [1, 0], [0, -1], [0, 1]];
  const possibleMoves: string[] = directionsHelper(directions, positions, rowIndex, colIndex);
  return possibleMoves;
};

export const bishopMoves = (
  positions: string[][],
  rowIndex: number,
  colIndex: number,
) => {
  const directions: number[][] = [[-1, -1], [-1, 1], [1, -1], [1, 1]];
  const possibleMoves: string[] = directionsHelper(directions, positions, rowIndex, colIndex);
  return possibleMoves;
};

export const knightMoves = (
  rowIndex: number,
  colIndex: number,
) => {
  const possibleMoves: string[] = [];
  const directions: number[][] = [[1, 2], [1, -2], [-1, 2], [-1, -2], [2, 1], [2, -1], [-2, 1], [-2, -1]];

  directions.forEach((direction) => {
    const rowMult: number = direction[0];
    const colMult: number = direction[1];
    const nextRow = rowIndex + (1 * rowMult);
    const nextCol = colIndex + (1 * colMult);
    if (nextRow >= 0 && nextRow <= 7 && nextCol >= 0 && nextCol <= 7) {
      possibleMoves.push(`${nextRow}${nextCol}`);
    }
  });

  return possibleMoves;
};
