import {
  FENPositionsToArray,
  arrayPositionsToFEN,
  updateTurn,
  updateNumberOfMoves,
} from './FENUtility';

test('FENPositionsToArray + arrayPositionsToFEN', () => {
  let positions: string[][] = FENPositionsToArray(['3', '3', '2P']);
  expect(positions[0][0]).toBe('0');
  expect(positions[2][1]).toBe('0');
  expect(positions[2][2]).toBe('P');
  let FEN: string = arrayPositionsToFEN(positions);
  expect(FEN).toBe('3/3/2P');

  positions = FENPositionsToArray(['q2', '3', '1P1']);
  expect(positions[0][0]).toBe('q');
  expect(positions[0][1]).toBe('0');
  expect(positions[2][0]).toBe('0');
  expect(positions[2][1]).toBe('P');
  expect(positions[2][2]).toBe('0');
  FEN = arrayPositionsToFEN(positions);
  expect(FEN).toBe('q2/3/1P1');
});

test('updateTurn', () => {
  let gameState = updateTurn(['w']);
  expect(gameState[0]).toBe('b');
  gameState = updateTurn(['b']);
  expect(gameState[0]).toBe('w');
});

test('updateNumberOfMoves', () => {
  const gameState = updateNumberOfMoves(['', '', '', '0', '1']);
  expect(gameState[3]).toBe('1');
  expect(gameState[4]).toBe('2');
});
