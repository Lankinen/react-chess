import { emptyArray } from '../../utils/ArrayUtils';
import {
  isWhiteSquare,
  isWhitePiece,
  isPromotion,
  setPromotionToBoard,
  isEmptySquare,
  movePiece,
  isLegalMove,
} from './GameUtility';

test('isWhiteSquare', () => {
  let isWhite: boolean = isWhiteSquare(0, 0);
  expect(isWhite).toBe(true);
  isWhite = isWhiteSquare(7, 7);
  expect(isWhite).toBe(true);
  isWhite = isWhiteSquare(1, 0);
  expect(isWhite).toBe(false);
});

test('isWhitePiece', () => {
  let isWhite: boolean = isWhitePiece('p');
  expect(isWhite).toBe(false);
  isWhite = isWhitePiece('P');
  expect(isWhite).toBe(true);
});

test('setPromotionToBoard', () => {
  let newPositions = setPromotionToBoard([['0', '?'], ['0', '0']], 'p');
  expect(newPositions[0][1]).toBe('p');
  newPositions = setPromotionToBoard([['0', '0'], ['0', '0']], 'p');
  expect(newPositions[0][1]).toBe('0');
});

test('isEmptySquare', () => {
  let isEmpty: boolean = isEmptySquare([['0', '0'], ['0', 'p']], 1, 0);
  expect(isEmpty).toBe(true);
  isEmpty = isEmptySquare([['0', '0'], ['0', 'p']], 1, 1);
  expect(isEmpty).toBe(false);
});

test('isPromotion', () => {
  expect(isPromotion(0, 'p')).toBe(true);
  expect(isPromotion(6, 'p')).toBe(false);
  expect(isPromotion(7, 'p')).toBe(true);
});

test('movePiece', () => {
  let positions: string[][] = [['P', '0', '0'], ['0', 'q', '0'], ['0', '0', '0']];
  let newPositions = movePiece(positions, 0, 0, 1, 0);
  expect(newPositions[0][0]).toBe('0');
  expect(newPositions[1][0]).toBe('P');
  expect(newPositions[1][1]).toBe('q');
  positions = [['P', '0', '0'], ['0', 'q', '0'], ['0', '0', '0']];
  newPositions = movePiece(positions, 0, 0, 1, 1);
  expect(newPositions[0][0]).toBe('0');
  expect(newPositions[1][1]).toBe('P');
});

test('isLegalMovePawn', () => {
  const positions = [['q', '0', '0'], ['0', 'P', '0'], ['0', '0', '0']];
  let isLegal: boolean = isLegalMove(positions, 'w', 'P11', 1, 1);
  expect(isLegal).toBe(false);
  isLegal = isLegalMove(positions, 'w', 'P11', 0, 1);
  expect(isLegal).toBe(true);
  isLegal = isLegalMove(positions, 'w', 'P11', 0, 0);
  expect(isLegal).toBe(true);
  isLegal = isLegalMove(positions, 'w', 'P11', 0, 2);
  expect(isLegal).toBe(false);
});

const createPositions = (piece: string) => emptyArray(8).map((_, rowIndex: number) => emptyArray(8).map((_, colIndex: number) => {
  if (rowIndex === 1 && colIndex === 1) {
    return piece;
  }
  if (rowIndex === 0 && colIndex === 0) {
    return 'q';
  }
  return '0';
}));

test('isLegalMoveQueen', () => {
  const positions: string[][] = createPositions('Q');
  let isLegal: boolean = isLegalMove(positions, 'w', 'Q11', 1, 1);
  expect(isLegal).toBe(false);
  isLegal = isLegalMove(positions, 'w', 'Q11', 0, 1);
  expect(isLegal).toBe(true);
  isLegal = isLegalMove(positions, 'w', 'Q11', 0, 0);
  expect(isLegal).toBe(true);
  isLegal = isLegalMove(positions, 'w', 'Q11', 0, 2);
  expect(isLegal).toBe(true);
  isLegal = isLegalMove(positions, 'w', 'Q11', 2, 2);
  expect(isLegal).toBe(true);
  isLegal = isLegalMove(positions, 'w', 'Q11', 1, 2);
  expect(isLegal).toBe(true);
});

test('isLegalMoveRook', () => {
  const positions: string[][] = createPositions('R');
  let isLegal: boolean = isLegalMove(positions, 'w', 'R11', 1, 1);
  expect(isLegal).toBe(false);
  isLegal = isLegalMove(positions, 'w', 'R11', 0, 1);
  expect(isLegal).toBe(true);
  isLegal = isLegalMove(positions, 'w', 'R11', 0, 0);
  expect(isLegal).toBe(false);
  isLegal = isLegalMove(positions, 'w', 'R11', 2, 1);
  expect(isLegal).toBe(true);
});

test('isLegalMoveBishop', () => {
  const positions: string[][] = createPositions('B');
  let isLegal: boolean = isLegalMove(positions, 'w', 'B11', 1, 1);
  expect(isLegal).toBe(false);
  isLegal = isLegalMove(positions, 'w', 'B11', 0, 1);
  expect(isLegal).toBe(false);
  isLegal = isLegalMove(positions, 'w', 'B11', 0, 0);
  expect(isLegal).toBe(true);
  isLegal = isLegalMove(positions, 'w', 'B11', 0, 2);
  expect(isLegal).toBe(true);
  isLegal = isLegalMove(positions, 'w', 'B11', 2, 2);
  expect(isLegal).toBe(true);
});

test('isLegalMoveKnight', () => {
  const positions: string[][] = createPositions('N');
  let isLegal: boolean = isLegalMove(positions, 'w', 'N11', 1, 1);
  expect(isLegal).toBe(false);
  isLegal = isLegalMove(positions, 'w', 'N11', 2, 3);
  expect(isLegal).toBe(true);
  isLegal = isLegalMove(positions, 'w', 'N11', 3, 2);
  expect(isLegal).toBe(true);
  isLegal = isLegalMove(positions, 'w', 'N11', 0, 2);
  expect(isLegal).toBe(false);
});
