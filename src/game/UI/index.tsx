import React, { useState } from 'react';

import {
  cloneDoubleArray,
  cloneArray,
  shuffleArray,
} from '../../utils/ArrayUtils';

import {
  FENPositionsToArray,
  arrayPositionsToFEN,
} from '../utils/FENUtility';

import {
  isEmptySquare,
} from '../utils/GameUtility';

import GameRunning from '../utils/GameRunning';

import Board from './Board';
import PromotionMenu from './PromotionMenu';
import Modal from './Modal';

const initialFEN = '8/2p5/8/8/8/8/8/8_w_KQkq_-_0_1';
const initialPositions = FENPositionsToArray(initialFEN.split('_')[0].split('/'));
const initialGameState = initialFEN.split('_').splice(1);

export default function Game() {
  const [positions, setPositions] = useState(cloneDoubleArray(initialPositions));
  const [gameState, setGameState] = useState(cloneArray(initialGameState));
  const [gameRunning, setGameRunning] = useState(GameRunning.STOP);
  const [selected, setSelected] = useState<string>('');

  const initializeGame = () => {
    setPositions(cloneDoubleArray(initialPositions));
    setGameState(cloneArray(initialGameState));
    setSelected('');
    setGameRunning(GameRunning.STOP);
  };

  const clickedStart = () => {
    const _positions = cloneArray(positions);
    if (gameRunning === GameRunning.START || gameRunning === GameRunning.WHITE_WON || gameRunning === GameRunning.BLACK_WON) {
      initializeGame();
    } else {
      const options: number[] = shuffleArray([0, 1, 2, 3, 4, 5, 6, 7]);
      while (options.length > 0) {
        const randomColumn: number = options.pop() || 0;
        if (isEmptySquare(_positions, 6, randomColumn)) {
          _positions[6][randomColumn] = 'P';
          setPositions(_positions);
          setGameRunning(GameRunning.START);
          break;
        }
      }
    }
  };

  const whiteWon: boolean = gameRunning === GameRunning.WHITE_WON;
  const blackWon: boolean = gameRunning === GameRunning.BLACK_WON;
  const wonComp = (whiteWon || blackWon) && (
    <div>
      <p>
        {whiteWon ? 'White' : 'Black'}
        {' '}
        won the game
      </p>
      <button type="button" style={styles.button} onClick={clickedStart}>Reset</button>
    </div>
  );

  const promComp = gameRunning === GameRunning.PROMOTION && (
    <PromotionMenu
      positions={positions}
      setPositions={(newPositions: string[][]) => {
        setPositions(newPositions);
        setGameRunning(GameRunning.START);
      }}
      turn={gameState[0]}
    />
  );

  return (
    <div style={styles.container}>
      <>
        <h3 style={{ marginBottom: 0 }}>
          FEN
          {' '}
          <a style={{ textDecoration: 'none' }} href="https://www.dcode.fr/fen-chess-notation" target="_blank" rel="noreferrer">ℹ️</a>
        </h3>
        <p style={{ marginTop: 5 }}>{`${arrayPositionsToFEN(positions)} ${gameState.join(' ')}`}</p>
      </>
      <div style={styles.center}>
        <Modal boxContent={wonComp || promComp}>
          <Board
            positions={positions}
            setPositions={setPositions}
            gameState={gameState}
            setGameState={setGameState}
            selected={selected}
            setSelected={setSelected}
            setGameRunning={setGameRunning}
          />
        </Modal>
      </div>
      <br />
      <button style={styles.button} type="button" onClick={clickedStart}>{gameRunning === GameRunning.START ? 'Reset' : 'Start'}</button>
    </div>
  );
}

const styles: any = {
  container: {
    textAlign: 'center',
  },
  center: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    borderRadius: 10,
    margin: 10,
    width: 120,
    height: 40,
  },
};
