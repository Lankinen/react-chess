import React, { useState } from 'react';
import { cloneArray, emptyArray } from '../../utils/ArrayUtils';
import useWindowSize from '../../hooks/useWindowSize';
import {
  isWhiteSquare,
  isWhitePiece,
  isLegalMove,
  movePiece,
  isPromotion,
} from '../utils/GameUtility';
import {
  updateTurn,
  updateNumberOfMoves,
} from '../utils/FENUtility';
import GameRunning from '../utils/GameRunning';

interface BoardProps {
  positions: string[][],
  setPositions: (newPosition: string[][]) => void,
  gameState: string[],
  setGameState: (newGameState: string[]) => void,
  selected: string,
  setSelected: (newSelected: string) => void,
  setGameRunning: (newGameRunning: GameRunning) => void
}

const DEFAULT_SQUARE_SIZE = 55;

export default function Board(props: BoardProps) {
  const {
    positions,
    setPositions,
    gameState,
    setGameState,
    selected,
    setSelected,
    setGameRunning,
  } = props;

  const windowSize = useWindowSize();

  const isTurnAndIsOwnPiece = (turn: string, piece: string) => (turn === 'b' && !isWhitePiece(piece)) || (turn === 'w' && isWhitePiece(piece));

  const clickedSquare = (piece: string, _isLegalMove: boolean, rowIndex: number, colIndex: number) => {
    const isPieceInSquare = piece !== '0';
    if (_isLegalMove) {
      const isCapture = isPieceInSquare;
      let _gameState = cloneArray(gameState);
      setSelected('');
      const fromRow = parseInt(selected.split('')[1], 10);
      const fromColumn = parseInt(selected.split('')[2], 10);
      if (isPromotion(rowIndex, positions[fromRow][fromColumn])) {
        setGameRunning(GameRunning.PROMOTION);
      }
      const _positions = movePiece(positions, fromRow, fromColumn, rowIndex, colIndex);
      setPositions(_positions);

      if (isCapture) {
        setGameRunning(_gameState[0] === 'w' ? GameRunning.WHITE_WON : GameRunning.BLACK_WON);
      }

      _gameState = updateTurn(_gameState);
      _gameState = updateNumberOfMoves(_gameState, isCapture);
      setGameState(_gameState);
    } else if (isPieceInSquare) {
      const turn: string = gameState[0];
      const newSelected: string = `${piece}${rowIndex}${colIndex}`;
      if (isTurnAndIsOwnPiece(turn, piece)) {
        if (newSelected === selected) {
          setSelected('');
        } else {
          setSelected(newSelected);
        }
      }
    }
  };

  const squareSize = windowSize.width < DEFAULT_SQUARE_SIZE * 9 ? windowSize.width / 9 : DEFAULT_SQUARE_SIZE;
  return (
    <>
      {emptyArray(8).map((_, rowIndex: number) => (
        <div key={rowIndex} style={styles.row}>
          {emptyArray(8).map((_, colIndex: number) => {
            const piece = positions[rowIndex][colIndex];
            const _isWhiteSquare = isWhiteSquare(rowIndex, colIndex);
            const _isLegalMove = isLegalMove(positions, gameState[0], selected, rowIndex, colIndex);
            return (
              <div
                key={colIndex}
                style={styles.square(squareSize, _isWhiteSquare, _isLegalMove)}
                onClick={() => clickedSquare(piece, _isLegalMove, rowIndex, colIndex)}
              >
                {piece !== '0' && piece !== '?' && (
                  <img
                    alt={`Piece ${piece}`}
                    src={`/pieces/${piece}.png`}
                    style={styles.piece(squareSize * 0.9)}
                  />
                )}
              </div>
            );
          })}
        </div>
      ))}
    </>
  );
}

const styles: any = {
  row: {
    display: 'table',
    tableLayout: 'fixed',
  },
  square: (size: number, isWhite: boolean, _isLegalMove: boolean) => ({
    backgroundColor: _isLegalMove ? 'orange' : (isWhite ? 'darkgrey' : 'olive'),
    width: size,
    height: size,
    display: 'table-cell',
    border: '1px black solid',
    textAlign: 'center',
  }),
  piece: (size: number) => ({
    width: size,
    height: size,
  }),
};
