import React, { useState } from 'react';

import { setPromotionToBoard } from '../utils/GameUtility';

interface PromotionMenuProps {
  positions: string[][],
  setPositions: (newPositions: string[][]) => void,
  turn: string
}

export default function PromotionMenu(props: PromotionMenuProps) {
  const {
    positions,
    setPositions,
    turn,
  } = props;

  const clickedButton = (piece: string) => {
    const newPromotionPiece = turn === 'b' ? piece.toUpperCase() : piece.toLowerCase();
    const newPositions = setPromotionToBoard(positions, newPromotionPiece);
    setPositions(newPositions);
  };

  return (
    <div>
      <h2>Promotion</h2>
      <button style={styles.button} type="button" onClick={() => clickedButton('q')}>Queen</button>
      <br />
      <button style={styles.button} type="button" onClick={() => clickedButton('r')}>Rook</button>
      <br />
      <button style={styles.button} type="button" onClick={() => clickedButton('n')}>Knight</button>
      <br />
      <button style={styles.button} type="button" onClick={() => clickedButton('b')}>Bishop</button>
    </div>
  );
}

const styles: any = {
  button: {
    borderRadius: 10,
    margin: 10,
    padding: 5,
    width: 90,
    height: 40,
  },
};
