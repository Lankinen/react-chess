import React from 'react';
import useWindowSize from '../../hooks/useWindowSize';

interface ModalProps {
  children: any,
  boxContent: any
}

const MODAL_WIDTH = 500;
const MODAL_HEIGHT = 400;

export default function Modal(props: ModalProps) {
  const { children, boxContent } = props;
  const windowSize = useWindowSize();
  const { height, width } = windowSize;

  if (!boxContent) {
    return (
      <div>
        {children}
      </div>
    );
  }

  return (
    <div style={styles.container}>
      {children}
      <div style={styles.background(width, height)}>
        <div style={styles.box(width, height)}>
          {boxContent}
        </div>
      </div>
    </div>
  );
}

const styles: any = {
  container: {
    alignItems: 'center',
  },
  background: (width: number, height: number) => ({
    position: 'absolute',
    left: 0,
    top: 0,
    width,
    height,
    backgroundColor: 'rgba(0,0,0,0.7)',
  }),
  box: (width: number, height: number) => ({
    position: 'absolute',
    left: (width / 2) - (MODAL_WIDTH / 2),
    top: (height / 2) - (MODAL_HEIGHT / 2),
    width: MODAL_WIDTH,
    height: MODAL_HEIGHT,
    backgroundColor: 'beige',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
  }),
};
