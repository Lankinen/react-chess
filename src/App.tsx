import React, { useState } from 'react';
import './App.css';
import Game from './game/UI/index';

export default function App() {
  return (
    <div style={styles.container}>
      <h1 style={styles.header}>React Chess</h1>
      <Game />
    </div>
  );
}

const styles: any = {
  container: {
    alignItems: 'center',
  },
  header: {
    textAlign: 'center',
  },
};
